Modules reference
=================

:mod:`satnogsclient`
--------------------

.. automodule:: satnogsclient


:mod:`satnogsclient.locator.locator`
------------------------------------

.. automodule:: satnogsclient.locator.locator


:mod:`satnogsclient.observer.commsocket`
----------------------------------------

.. automodule:: satnogsclient.observer.commsocket


:mod:`satnogsclient.observer.observer`
--------------------------------------

.. automodule:: satnogsclient.observer.observer


:mod:`satnogsclient.observer.orbital`
-------------------------------------

.. automodule:: satnogsclient.observer.orbital


:mod:`satnogsclient.observer.waterfall`
---------------------------------------

.. automodule:: satnogsclient.observer.waterfall


:mod:`satnogsclient.observer.worker`
------------------------------------

.. automodule:: satnogsclient.observer.worker


:mod:`satnogsclient.radio`
--------------------------

.. automodule:: satnogsclient.radio


:mod:`satnogsclient.rotator`
----------------------------

.. automodule:: satnogsclient.rotator


:mod:`satnogsclient.scheduler.tasks`
------------------------------------

.. automodule:: satnogsclient.scheduler.tasks


:mod:`satnogsclient.settings`
-----------------------------

.. automodule:: satnogsclient.settings


:mod:`satnogsclient.upsat.gnuradio_handler`
-------------------------------------------

.. automodule:: satnogsclient.upsat.gnuradio_handler
